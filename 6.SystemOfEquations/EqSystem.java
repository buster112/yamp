 import java.util.Scanner; 
 import java.lang.Math;
 
  class EqSystem {
	  public static void main(String[] args) {
		  double a1,a2,b1,b2,s1,s2,d,r1,r2;
		  Scanner sc = new Scanner(System.in);
		  
		  System.out.println("Enter coefficients of the 1st equation: a1,b1,s1");
		  a1 = sc.nextDouble();
		  b1 = sc.nextDouble();
		  s1 = sc.nextDouble();
		  
		  System.out.println("Enter coefficients of the 1st equation: a2,b2,s2");
		  a2 = sc.nextDouble();
		  b2 = sc.nextDouble();
		  s2 = sc.nextDouble();
		  
		  d = a1 * b2 - a2 * b1;
		  if (d == 0) {
			  System.out.println("System has infinite number of roots or has no roots");
		  }
		  else {
			  r1 = (s1 * b2 - s2 * b1) / d;
			  r2 = (a1 * s2 - a2 * s1) / d;
			  System.out.printf("System has 2 roots. It is %6.3f and %6.3f", r1, r2);
		  }
	  }
  }