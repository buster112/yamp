 import java.util.Scanner;
  
  class Arrays {
	  public static void main (String[] args) {
		  
		  Scanner sc = new Scanner(System.in);
		  int n;
		 
		  System.out.println("Enter the number of elements: ");
		  n = sc.nextInt();
		  int myArray[] = new int[n];
		 
   		  FillArr(n, myArray);
		  ShowArr(myArray);
		  ArrSum(myArray);
		  CheckEven(myArray);
		  IsItInAB(myArray);
		  isPositive(myArray);
		  ReverseArr(myArray);
		  ShowArr(myArray);
		  
	  }
	  
	  public static void FillArr(int n, int array[]) {
		  Scanner sc = new Scanner(System.in);
		  int item;
		  for (int i = 0; i < array.length; i++) {
			  System.out.println("Enter the " + (i+1) + " element: ");
			  item = sc.nextInt();
			  array[i] = item;
		  }
	  }
	  
	  public static void ShowArr(int array[]) {
		  System.out.println("Current array:");
		  for (int i = 0; i < array.length; i++) {
			  System.out.print(array[i] + " ");
		  }
		  System.out.println();
	  }
	  
	  public static void ArrSum(int array[]) {
		  int sum = 0;
		  for (int i = 0; i < array.length; i++) {
			  sum += array[i];
		  }
		  System.out.println("Sum of elements = " + sum);
	  }
	  
	  public static void CheckEven(int array[]) {
		  int count = 0;
		  for (int i = 0; i < array.length; i++) {
			  if (array[i] % 2 == 0) {
				  count++;
			  }
		  }
		  System.out.println("A number of even numbers is " + count);
	  }
	  
	  public static void IsItInAB(int array[]) {
		  int a,b;
		  Scanner sc = new Scanner(System.in);
		  int count = 0;
		  
		  System.out.println("Enter the segment points (a,b): ");
		  a = sc.nextInt();
		  b = sc.nextInt();
		  for (int i = 0; i < array.length; i++) {
			  if ((array[i] <= b) && (array[i] >= a) ) {
				  count++;
			  }
		  }
		  System.out.println("A number of numbers from segment [" + a + ", " + b + "] is " + count);
	  }
	  
	  public static void isPositive(int array[]) {
	      boolean check = true;
	  
	      for (int i = 0; i < array.length; i++) {
			  if (array[i] <= 0) {
				  check = false;
			    }
			}
		  if (check == true) {
			  System.out.println("All the elements are positive");
		   }
		  else {
			  System.out.println("Not all elements are positive");
		    }
        }  
  
	  public static void ReverseArr(int array[]) {
		  int temp;
	  
	      for (int i = 0; i <= Math.floor(array.length / 2) ; i++) {
		     temp = array[i];
		     array[i] = array[array.length - 1 - i];
		     array[array.length - 1 - i] = temp;
	        }
        }
  
  }