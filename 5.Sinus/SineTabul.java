 import java.util.Scanner; 
 import java.lang.Math;
  
  class SineTabul {
	  public static void main(String[] args) {
		  
		  double step,start,finish;
		  Scanner sc = new Scanner (System.in);
		  
		  System.out.println("Enter start (in degrees)");
		  start = sc.nextDouble();
		  
		  System.out.println("Enter finish (in degrees)");
		  finish = sc.nextDouble();
		  
		  System.out.println("Enter the step (in degrees)");
		  step = sc.nextDouble();
		  
		  System.out.println("|     X     |   Sin(X)  |");
		  System.out.println("|-----------|-----------|");
		  for (double i = start; i <= finish; i += step) {
	     	 System.out.printf("|%10.3f | %+10.4f|%n", i, Math.sin(i));
		  }
	  }
  }