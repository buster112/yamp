 package ru.omsu.imit.suzenko.quadratic.equations;
 import java.util.Scanner; 
 import java.lang.Math;
 class Equations {
	 public static void main(String[] args) {
		 
		 double a,b,c;
		 double t[];
		 Scanner sc = new Scanner(System.in);
		 t = new double[3];
		 
		 for (int i = 0; i <= 2; i++) {
				 System.out.println("Vvedite " + (i+1) + " chislo.");
				 t[i] = sc.nextDouble();
		 }
		 
		EqRoots(t[0],t[1],t[2]);
		
	 }
	 
	 public static void EqRoots(double a, double b, double c) {
		 double D,r1,r2;
		 D = b * b - 4 * a * c;
		 if (D < 0) {
			 System.out.println("Equation hasn't real roots.");
		 }
		 else if (D > 0) {
		 r1 = (b * (-1) + Math.sqrt(D)) / 2 * a;
		 r2 = (b * (-1) - Math.sqrt(D)) / 2 * a;
		 System.out.println("Equation has 2 roots. It is " + r1 + " and " + r2);
		 }
		 else {
			 r1 = (b * (-1) + Math.sqrt(D)) / 2 * a;
			 System.out.println("Equation has 2 equal roots. It is " + r1);
		 }
	 }
 }