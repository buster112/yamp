import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by secto on 26.11.2016.
 */
public class Var18Test {

    @Test
    public void test1() {
        String s1 = "Hello it is test";
        String s2 = "I don't k now how to write a test";
        assertEquals("test ",Variant18.FindWords(s1,s2));
        assertEquals("",Variant18.FindWords("123 321 ss 11","3321 sdf 24 1"));
    }
}
