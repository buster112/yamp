/**
 * Created by secto on 26.11.2016.
 */
import java.util.HashSet;
import java.util.Scanner;

public class Variant18 {

    /**
     * Method finds words that appear in each of two strings
     * @param s1
     * @param s2
     * @return
     */
    public static String FindWords(String s1,String s2) {
        String parts1[] = s1.split(" ");
        String parts2[] = s2.split(" ");
        StringBuilder answer = new StringBuilder();

        HashSet<String> str2Set = new HashSet<>();
        for (String aParts21 : parts2) {
            str2Set.add(aParts21);
        }

        for (String aParts1 : parts1) {
                if (str2Set.contains(aParts1)) {
                    answer.append(aParts1 + " ");
                }
        }

        return answer.toString();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s1,s2;
        System.out.println("Введите первую строку:\n");
        s1 = sc.nextLine();
        System.out.println("Введите вторую строку:\n");
        s2 = sc.nextLine();
        System.out.println("Слова, встречающиеся в обеих строках: \n" + Variant18.FindWords(s1,s2));
    }
}
