package ru.omsu.imit.Suzenko.matrix;

/**
 * Created by secto on 15.02.2017.
 */
public interface IInvertableMatrix extends IMatrix {
    IMatrix getInvertedMatrix() throws Exception;
}
