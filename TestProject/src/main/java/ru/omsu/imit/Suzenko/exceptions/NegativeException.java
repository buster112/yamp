package ru.omsu.imit.Suzenko.exceptions;

/**
 * Created by secto on 29.10.2016.
 */
public class NegativeException extends Exception{
    public NegativeException(String str) {
        super(str);
    }
}
