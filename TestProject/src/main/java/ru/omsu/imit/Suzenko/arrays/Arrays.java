package ru.omsu.imit.Suzenko.arrays;
import java.util.Scanner;

  public class Arrays {
	  public static void main (String[] args) {
		  
		  Scanner sc = new Scanner(System.in);
		  int n;
		  int a = -3 ,b = 3;
		  System.out.println("Enter the number of elements: ");
		  n = sc.nextInt();
		  int myArray[] = new int[n];
		 
   		  FillArr(n, myArray);
		  ShowArr(myArray);
		  ArrSum(myArray);
		  CheckEven(myArray);
		  IsItInAB(myArray,a,b);
		  isPositive(myArray);
		  ReverseArr(myArray);
		  ShowArr(myArray);
		  
	  }
	  
	  public static void FillArr(int n, int array[]) {
		  Scanner sc = new Scanner(System.in);
		  int item;
		  for (int i = 0; i < array.length; i++) {
			  System.out.println("Enter the " + (i+1) + " element: ");
			  item = sc.nextInt();
			  array[i] = item;
		  }
	  }
	  
	  public static void ShowArr(int array[]) {
		  System.out.println("Current array:");
		  for (int i = 0; i < array.length; i++) {
			  System.out.print(array[i] + " ");
		  }
		  System.out.println();
	  }
	  
	  public static int ArrSum(int array[]) {
		  int sum = 0;
		  for (int i = 0; i < array.length; i++) {
			  sum += array[i];
		  }
		  return(sum);
		  //System.out.println("FunctionalSum of elements = " + sum);
	  }
	  
	  public static int CheckEven(int array[]) {
		  int count = 0;
		  for (int i = 0; i < array.length; i++) {
			  if (array[i] % 2 == 0) {
				  count++;
			  }
		  }
		  //System.out.println("A number of even numbers is " + count);
		  return (count);
	  }
	  
	  public static int IsItInAB(int array[],int a,int b) {
		 /* int a,b;
		  Scanner sc = new Scanner(System.in);

		  System.out.println("Enter the segment points (a,b): ");
		  a = sc.nextInt();
		  b = sc.nextInt(); */
		  int count = 0;
		  for (int i = 0; i < array.length; i++) {
			  if ((array[i] <= b) && (array[i] >= a) ) {
				  count++;
			  }
		  }
		  //System.out.println("A number of numbers from segment [" + a + ", " + b + "] is " + count);
		  return (count);
	  }
	  
	  public static boolean isPositive(int array[]) {
	      boolean check = true;
	  
	      for (int i = 0; i < array.length; i++) {
			  if (array[i] <= 0) {
				  check = false;
			    }
			}
		  if (check == true) {
			  //System.out.println("All the elements are positive");
			  return(check);
		   }
		  else {
			  //System.out.println("Not all elements are positive");
			  return(check);
		    }
        }  
  
	  public static int[] ReverseArr(int array[]) {
		  int temp;
	  
	      for (int i = 0; i <= Math.floor(array.length / 2) ; i++) {
		     temp = array[i];
		     array[i] = array[array.length - 1 - i];
		     array[array.length - 1 - i] = temp;
	        }
		  return (array);
        }
  
  }