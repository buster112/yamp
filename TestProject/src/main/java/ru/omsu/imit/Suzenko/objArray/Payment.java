package ru.omsu.imit.Suzenko.objArray;

import java.util.Objects;

/**
 * Created by secto on 12.11.2016.
 */
public class Payment implements Comparable<Payment>{
    private String FIO;
    private int dayOfPay;
    private int monthOfPay;
    private int yearOfPay;
    private int amountOfPay;

    public Payment() {
    }

    public Payment(Payment p) {
        this.FIO = new String(p.getFIO());
        this.dayOfPay = p.getDayOfPay();
        this.monthOfPay = p.getMonthOfPay();
        this.yearOfPay = p.getYearOfPay();
        this.amountOfPay = p.getAmountOfPay();
    }

    public Payment(String name, int day, int month, int year, int amount) {
        FIO = name;
        dayOfPay = day;
        monthOfPay = month;
        yearOfPay = year;
        amountOfPay = amount;
    }

    public String getFIO() {
        return FIO;
    }

    public int getDayOfPay() {
        return dayOfPay;
    }

    public int getYearOfPay() {
        return yearOfPay;
    }

    public int getAmountOfPay() {
        return amountOfPay;
    }

    public int getMonthOfPay(){
        return monthOfPay;
    }

    public void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public void setDayOfPay(int dayOfPay) {
        this.dayOfPay = dayOfPay;
    }

    public void setMonthOfPay(int monthOfPay) {
        this.monthOfPay = monthOfPay;
    }

    public void setYearOfPay(int yearOfPay) {
        this.yearOfPay = yearOfPay;
    }

    public void setAmountOfPay(int amountOfPay) {
        this.amountOfPay = amountOfPay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return dayOfPay == payment.dayOfPay &&
                monthOfPay == payment.monthOfPay &&
                yearOfPay == payment.yearOfPay &&
                amountOfPay == payment.amountOfPay &&
                Objects.equals(FIO, payment.FIO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(FIO, dayOfPay, monthOfPay, yearOfPay, amountOfPay);
    }

    @Override
    public String toString() {
        return "Name: " + FIO +
                ", Day of pay: " + dayOfPay +
                ", month of pay: " + monthOfPay +
                ", year of pay: " + yearOfPay +
                ", amount of pay: " + amountOfPay ;
    }

    @Override
    public int compareTo(Payment payment) {
        if (FIO == payment.FIO && dayOfPay == payment.dayOfPay && amountOfPay == payment.amountOfPay){
            return 1 ;
        }
        else return 0;
    }
}
