package ru.omsu.imit.Suzenko.functions;

import ru.omsu.imit.Suzenko.exceptions.OverstepException;


/**
 * Created by secto on 12.04.2017.
 */
public class PolynomDivide implements Function{
    private double a;
    private double b;
    private double A;
    private double B;
    private double C;
    private double D;

    public PolynomDivide(double a, double b, double A, double B, double C, double D){
        this.a = a;
        this.b = b;
        this.A = A;
        this.B = B;
        this.C = C;
        this.D = D;
    }

    public double getA(){
        return a;
    }

    public double getB(){
        return b;
    }

    public double function(double x) throws OverstepException {
        double result = (A * x + B)/(C * x + D);
        if (x < a || x > b) throw new OverstepException("Overstep Exception");
        else return result;
    }
}
