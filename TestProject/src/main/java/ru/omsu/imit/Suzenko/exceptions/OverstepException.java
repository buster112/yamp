package ru.omsu.imit.Suzenko.exceptions;

/**
 * Created by secto on 12.04.2017.
 */
public class OverstepException extends Exception {
    public OverstepException(String message) {
        super(message);
    }
}
