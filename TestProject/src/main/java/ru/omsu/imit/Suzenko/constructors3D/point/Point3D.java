package ru.omsu.imit.Suzenko.constructors3D.point;

/**
 * Created by secto on 01.10.2016.   TextForCommit: 123
 */
public class Point3D {
    private double x;
    private double y;
    private double z;

    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point3D() {
        x = 0;
        y = 0;
        z = 0;
    }

    public void setX(double x) {
        x = this.x;
    }
    public void setY(double y) {
        y = this.y;
    }
    public void setZ(double z) {
        z = this.z;
    }

    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public double getZ() {
        return z;
    }

    public void print() {
        System.out.println("x = " + x + " y = " + y + " z = " + z);
    }

    public boolean equals(Point3D point) {
        final double Eps = 0.0001;
        if (this == point ) return true;
        if ((Math.abs(x - point.getX()) < Eps )&&(Math.abs(y - point.getY()) < Eps )&&(Math.abs(z - point.getZ()) < Eps ))
            return true;
        else return false;
    }
}
