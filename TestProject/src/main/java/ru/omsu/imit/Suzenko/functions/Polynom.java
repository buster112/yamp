package ru.omsu.imit.Suzenko.functions;

import ru.omsu.imit.Suzenko.exceptions.OverstepException;

/**
 * Created by secto on 12.04.2017.
 */
public class Polynom implements Function{
    private double a;
    private double b;
    private double A;
    private double B;

    public Polynom(double a, double b, double A, double B){
        this.a = a;
        this.b = b;
        this.A = A;
        this.B = B;
    }

    public double getA(){
        return a;
    }

    public double getB(){
        return b;
    }

    public double function(double x) throws OverstepException {
        double result = A * x + B;
        return result;
    }

}
