package ru.omsu.imit.Suzenko.generics;

/**
 * Created by secto on 17.12.2016.
 */
public class MinMaxCalculator {
    public static <T extends Comparable<? super T>> Pair<T> getMinMaxPair(T[] array) {
        if (array.length == 0) {
            return new Pair<>(null, null);
        }

        Pair<T> result = new Pair<>(array[0], array[0]);

        for (T t : array) {
            if (t.compareTo(result.getFirst()) < 0) {
                result.setFirst(t);
            }

            if (t.compareTo(result.getSecond()) > 0) {
                result.setSecond(t);
            }
        }

        return result;
    }
    public static <T extends Comparable<? super T>> T getMin(Pair<T> pair){
        if (pair.getFirst().compareTo(pair.getSecond()) < 0) {
            return pair.getSecond();
        }
        else if (pair.getFirst().compareTo(pair.getSecond()) > 0) {
            return pair.getFirst();
        }
        else return null;
    }

    public static <T extends Comparable<? super T>> T getMax(Pair<T> pair){
        if (pair.getFirst().compareTo(pair.getSecond()) > 0) {
            return pair.getSecond();
        }
        else if (pair.getFirst().compareTo(pair.getSecond()) < 0) {
            return pair.getFirst();
        }
        else return null;
    }
}
