package ru.omsu.imit.Suzenko.generics;

import static javafx.scene.input.KeyCode.T;
/**
 * Created by secto on 03.12.2016.
 */
public class Figure implements Comparable<Figure> {
    private double square;

    public double getSquare() {
        return square;
    }

    public Figure(double square) {
        this.square = square;
    }

    public int compareTo(Figure o) {
        if (getSquare() - o.getSquare() < 0.000001) return -1;
        else if (getSquare() - o.getSquare() > 0.000001) return 1;
        else return 0;
    }
}
