package ru.omsu.imit.Suzenko.constructors3D.vector;

/**
 * Created by secto on 01.10.2016.
 */
public class Vector3DProcessor {

    public static Vector3D vectorSum(Vector3D v1, Vector3D v2) {
        Vector3D sum = new Vector3D(v1.getX() + v2.getX(), v1.getY() + v2.getY(), v1.getZ() + v2.getZ());
        return sum;
    }

    public static Vector3D vectorSubtraction(Vector3D v1, Vector3D v2) {
        Vector3D subtraction = new Vector3D(v1.getX() - v2.getX(), v1.getY() - v2.getY(), v1.getZ() - v2.getZ());
        return subtraction;
    }

    public static double scalarComposition(Vector3D v1, Vector3D v2) {
        double composition = (v1.getX() * v2.getX() + v1.getY() * v2.getY() + v1.getZ() * v2.getZ());
        return composition;
    }

    public static Vector3D vectorComposition(Vector3D v1, Vector3D v2) {
        double x1 = v1.getY() * v2.getZ() - v1.getZ() * v2.getY();
        double y1 = v1.getZ() * v2.getX() - v1.getX() * v2.getZ();
        double z1 = v1.getX() * v2.getY() - v1.getY() * v2.getX();
        Vector3D composition = new Vector3D(x1,y1,z1);
        return composition;
    }

    public static boolean isCollinear(Vector3D v1, Vector3D v2) {
        Vector3D v3 = new Vector3D(0,0,0);
        if (vectorComposition(v1,v2).equals(v3)) return true;
        else return false;
    }
}
