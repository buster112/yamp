package ru.omsu.imit.Suzenko.matrix;

/**
 * Created by secto on 15.02.2017.
 */
public interface IMatrix {
    double getElem(int i, int j);
    void setElem(int i, int j, double v);
    double getDeterminant();
}
