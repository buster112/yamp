package ru.omsu.imit.Suzenko.functional;

import ru.omsu.imit.Suzenko.exceptions.OverstepException;
import ru.omsu.imit.Suzenko.functions.Function;

/**
 * Created by secto on 19.04.2017.
 */
public class Integral implements Functional{

    public double functional(Function function) throws OverstepException {
        int n = 100;
        double a = function.getA();
        double b = function.getB();
        double step = b / n ;
        double sub;
        double mid;
        double integral = 0;
        b = step;
        for (int i = 0; i < n; i++){
            mid = (a + b) / 2;
            sub = b - a;
            integral += function.function(mid)*sub;
            a = b;
            b += step;
        }
        return integral;
    }
}
