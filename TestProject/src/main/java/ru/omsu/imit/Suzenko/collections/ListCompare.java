package ru.omsu.imit.Suzenko.collections;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by secto on 10.05.2017.
 */
public class ListCompare {
    ArrayList arrayList = new ArrayList();
    LinkedList linkedList = new LinkedList();
    public static void compareLists(ArrayList arrayList, LinkedList linkedList) {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            arrayList.add(Math.random());
        }
        long timeSpent = System.currentTimeMillis() - startTime;
        System.out.println("ArrayList = " + timeSpent);

        startTime = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            linkedList.add(Math.random());
        }
        timeSpent = System.currentTimeMillis() - startTime;
        System.out.println("LinkedList = " + timeSpent);
    }

    public static void main(String[] args) {
        ArrayList arrayList = new ArrayList();
        LinkedList linkedList = new LinkedList();
        compareLists(arrayList,linkedList);
    }

}
