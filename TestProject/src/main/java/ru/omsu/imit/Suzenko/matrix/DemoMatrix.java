package ru.omsu.imit.Suzenko.matrix;

import java.io.*;

/**
 * Created by secto on 22.03.2017.
 */
public class DemoMatrix {

    public static void main(String[] args) throws IOException, ClassNotFoundException  {
        double[] arr = {1,2,3,4,5,6,7,8,9};
        Matrix matrix1 = new Matrix(3, arr);
        Matrix matrix2 = new Matrix(3);
        Matrix matrix3;

        DemoMatrix.writeTextFile(matrix1);
        DemoMatrix.readTextFile(matrix2);
        matrix2.showMatrix();
        System.out.println("FunctionalSum of elems = " + DemoMatrix.sumOfElems(matrix1));
        DemoMatrix.serialize(matrix1);
        matrix3 = DemoMatrix.deSerialize();
        matrix3.showMatrix();
    }


    public static void writeTextFile(Matrix matrix) throws IOException {
        FileWriter writer = new FileWriter("F://Java Projects//yamp//TestProject//src//main//java//ru//omsu//imit//Suzenko//matrix//matrix_files//file.txt" /*"D://folder//file.txt"*/ , false);
        for (int i = 0; i < matrix.getSize(); i++) {
            for (int j = 0; j < matrix.getSize(); j++) {
                writer.write(String.valueOf(matrix.getElem(i, j)) + " ");
            }
            writer.write("\n");
        }
        writer.flush();
    }


    public static void readTextFile(Matrix m) throws IOException {
        String str = "";
        Reader reader = new FileReader(/*"D://folder//file.txt"*/"F://Java Projects//yamp//TestProject//src//main//java//ru//omsu//imit//Suzenko//matrix//matrix_files//file.txt");
        int data = reader.read();
        int c = 0;
        while(data != -1){
            if (c == 0){
                c++;
                str += (char) data;
            }
            data = reader.read();
            str += (char) data;
        }
        String[] arr = str.split("\n");
        str = "";
        for (int i = 0; i < arr.length; i++){
            str+= arr[i];
        }
        String[] parts = str.split(" ");
        double temp;
        for (int i = 0; i < parts.length - 1; i++){
            temp = Double.parseDouble(parts[i]);
            m.set(i, temp);
        }

    }


    public static double sumOfElems(Matrix matrix){
        double sum = 0;
        for (int i = 0; i < matrix.getSize(); i++){
            for (int j = 0; j < matrix.getSize(); j++){
                sum += matrix.getElem(i, j);
            }
        }
        return sum;
    }

    public static void serialize(Matrix matrix) throws IOException {
        FileOutputStream fos = new FileOutputStream("temp.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(matrix);
        oos.flush();
        oos.close();
    }

    public static Matrix deSerialize() throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream("temp.out");
        ObjectInputStream oin = new ObjectInputStream(fis);
        Matrix matrix = (Matrix) oin.readObject();
        return matrix;
    }


}