package ru.omsu.imit.Suzenko.generics;

/**
 * Created by secto on 17.12.2016.
 */
public class Rectangle  extends Figure{
    private double a;
    private double b;

    public Rectangle(double a, double b){
        super(a * b);
    }
}
