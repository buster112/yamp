package ru.omsu.imit.Suzenko.reflections;

import ru.omsu.imit.Suzenko.collections.Human;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

class ReflectionDemo {
	/*
	11. Дан список объектов произвольных типов.
	Найдите количество элементов списка, которые являются объектами типа Human или его подтипами.
	*/

    public static int countPeople(List<Object> list) {
        int count = 0;
        for (Object o : list) {
            if (o instanceof Human) {
                count++;
            }
        }
        return count;
    }


	/*
	12. Для объекта получить список имен его открытых методов.
	 */

    public static List<String> getPublicMethods(Object o) {
        Method[] methods = o.getClass().getMethods();
        ArrayList<String> result = new ArrayList<>(methods.length);

        for (Method m : methods) {
            if (Modifier.isPublic(m.getModifiers())) {
                result.add(m.getName());
            }
        }

        return result;
    }


	/*
	13. Для объекта получить список (в виде списка строк) имен всех его суперклассов
	до класса Object включительно.
	*/

    public static List<String> getParents(Object o) {
        ArrayList<String> parents = new ArrayList<>();
        Class currentSuper = o.getClass().getSuperclass();

        while (currentSuper != null) {
            parents.add(currentSuper.getSimpleName());
            currentSuper = currentSuper.getSuperclass();
        }

        return parents;
    }
}
