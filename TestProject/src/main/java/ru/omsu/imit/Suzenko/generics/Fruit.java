package ru.omsu.imit.Suzenko.generics;

/**
 * Created by secto on 03.12.2016.
 */
public class Fruit implements Comparable<Fruit> {

    private double weight;

    public Fruit(double weight) {
        this.weight = weight;
    }

    public double getWeight(){
        return weight;
    }

    public int compareTo(Fruit o) {
        if (getWeight() - o.getWeight() < 0.000001) return -1;
        else if (getWeight() - o.getWeight() > 0.000001) return 1;
        else return 0;
    }
}
