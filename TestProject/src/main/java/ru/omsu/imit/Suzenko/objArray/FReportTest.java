package ru.omsu.imit.Suzenko.objArray;

import java.util.Scanner;

/**
 * Created by secto on 10.12.2016.
 */
public class FReportTest {

    public static void main(String[] args) {
       /* FinanceReport report1 = new FinanceReport();
        report1.addPayment();
        report1.addPayment();
        System.out.println("report 1:");
        report1.showPayments();
        FinanceReport report2 = new FinanceReport(report1);
        System.out.println("report 2:");
        report2.showPayments();
        report1.changePayment();
        report1.showPayments();
        report2.showPayments(); */
        boolean check = true;
        FinanceReport report = new FinanceReport();
        Scanner sc = new Scanner(System.in);
        while (check) {
            System.out.println("1. Add payment\n2. Show payment\n3. Change payment\n4. Show all payments\n5. Exit");
            int i = sc.nextInt();
            if (i == 1) report.addPayment();
            else if (i == 2) report.showPayment();
            else if (i == 3) report.changePayment();
            else if (i == 4) report.showPayments();
            else if (i == 5) check = false;
        }
    }

}
