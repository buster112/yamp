package ru.omsu.imit.Suzenko.math;

/**
 * Created by secto on 17.09.2016.
 */
public class Calculation {

    public static double taylorCalc(double power, double accuracy) {
        double answer = 1;
        double f = 1;
        for (int i = 1; Math.abs(f) > accuracy; i++) {
            f *= power/i;
            answer += f;
        }
        return(answer);
    }
}
