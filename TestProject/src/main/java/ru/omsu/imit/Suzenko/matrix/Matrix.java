package ru.omsu.imit.Suzenko.matrix;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

/**
 * Created by secto on 15.02.2017.
 */
public class Matrix implements IMatrix, Serializable{
    private double[] matrix;
    protected double[] matrixCopy;
    protected final double EPS = 0.000001;
    private int n;
    private double determinant;
    private boolean detFlag;

    public Matrix (int n) {
        matrix = new double[n * n];
        this.n = n;
    }

    public Matrix (int n, double[] array){
        matrix = new double[n * n];
        for (int i = 0; i < n * n; i++) {
            matrix[i] = array[i];
        }
        this.n = n;
    }

    public int getSize(){
        return n;
    }

    public double getElem(int i, int j){
        int index = n * i + j;
        return matrix[index];
    }

    public void setElem(int i, int j, double v){
        int index = n * i + j;
        if (matrix[index] != v) {
            detFlag = false;
        }
        matrix[index] = v;
    }

    protected void set(int i, double elem) {
        if (matrix[i] != elem) {
            detFlag = false;
        }
        matrix[i] = elem;
    }

    protected double getTempElem(int i, int j){
        int index = n * i + j;
        return matrixCopy[index];
    }

    protected void setTempElem(int i, int j, double v){
        matrixCopy[n * i + j] = v;
    }

    public void setMatrix(double[] arr){
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                matrix[n * i + j] = arr[n * i + j];
            }
        }
    }

    public void showMatrix(){
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                System.out.print(getElem(i, j) + " ");
            }
            System.out.println();
        }
    }

    public void copyMatrix() {
        matrixCopy = new double[n * n];
        System.arraycopy(matrix,0,matrixCopy,0,n*n);
    }

    public void swapLines(int k, int l){
        double temp;
        for (int i = 0; i < n;i++){
            temp = getTempElem(k, i);
            setTempElem(k, i, getTempElem(l, i));
            setTempElem(l, i, temp);
        }
    }

    protected int findNonZeroString(int j){
        for (int i = j + 1; i < n; i++){
            if (Math.abs(getTempElem(i, j)) > EPS){
                return i;
            }
        }
        return -1;
    }

    protected void gauss(int k, int l, double coef){
        for (int i = 0; i < n; i++){
            matrixCopy[n * l + i] -= matrixCopy[n * k + i] * coef;
        }
    }


    private double det(){
        copyMatrix();
        double det = 1;
        for (int x = 0; x < n - 1; x++){
            if (Math.abs(getTempElem(x, x)) < EPS ){
                int check = findNonZeroString(x);
                if (check == -1) {
                    return 0;
                }
                swapLines(x, check);
                det = -det;
            }
            for (int i = x + 1; i < n; i++){
                double coef = getTempElem(i, x) / getTempElem(x, x);
                gauss(x, i, coef);
            }
        }
        for (int i = 0; i < n; i++){
            det *= getTempElem(i, i);
        }
        determinant = det;
        detFlag = true;
        return det;
    }

    public double getDeterminant() {
        if (detFlag) return determinant;
        else return det();
    }

    public boolean checkFlag() {
        return detFlag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Matrix matrix1 = (Matrix) o;

        if (n != matrix1.n) return false;
        return Arrays.equals(matrix, matrix1.matrix);
    }


    @Override
    public int hashCode() {
        return Objects.hash(matrix, n);
    }
}
