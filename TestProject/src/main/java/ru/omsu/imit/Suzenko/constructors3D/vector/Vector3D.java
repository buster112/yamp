package ru.omsu.imit.Suzenko.constructors3D.vector;
import ru.omsu.imit.Suzenko.constructors3D.point.*;


/**
 * Created by secto on 01.10.2016.
 */
public class Vector3D {
    private double x;
    private double y;
    private double z;

    public Vector3D() {
        x = 0;
        y = 0;
        z = 0;
    }
    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Vector3D(Point3D p1, Point3D p2) {
        this.x = (p2.getX() - p1.getX());
        this.y = (p2.getY() - p1.getY());
        this.z = (p2.getZ() - p1.getZ());
    }
    public double distance() {
        double result = (Math.sqrt(x * x + y * y + z * z));
        return result;
    }

    public void setX(double x){
        this.x = x;
    }
    public void setY(double y){
        this.y = y;
    }
    public void setZ(double z){
        this.z = z;
    }

    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public double getZ() {
        return z;
    }

    public boolean equals(Vector3D vector) {
        final double Eps = 1e-4;
        if (this == vector ) return true;
        return ((Math.abs(x - vector.getX()) < Eps )&&(Math.abs(y - vector.getY()) < Eps )&&(Math.abs(z - vector.getZ()) < Eps ));
    }
}
