package ru.omsu.imit.Suzenko.functional;

import ru.omsu.imit.Suzenko.exceptions.OverstepException;
import ru.omsu.imit.Suzenko.functions.Function;

/**
 * Created by secto on 19.04.2017.
 */
public class FunctionalSum implements Functional{

    public double functional(Function function) throws OverstepException {
        double sum = 0;
        double mid = (function.getA() + function.getB()) / 2;
        sum = function.getA() + function.getB() + mid;
        return sum;
    }
}
