package ru.omsu.imit.Suzenko.string;

import ru.omsu.imit.Suzenko.exceptions.NegativeException;

/**
 * Created by Vlad on 19.10.2016.
 */
public class StringProcessor {

    public static String revert(String s) {
        StringBuilder builder = new StringBuilder();
        for (int i = s.length() - 1; i >= 0; i--) {
            builder.append(s.charAt(i));
        }
        return builder.toString();
    }

    public static String intArrToString(int[] arr) {
        //  String s = new String();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            if (i < arr.length - 1) {
                builder.append(arr[i] + " ");
            } else {
                builder.append(arr[i]);
            }
        }
        return builder.toString();
    }

    public static int[] stringToArr(String s) {
        String[] parts = s.split(" ");
        int[] arr = new int[parts.length];
        for (int i = 0; i < parts.length; i++) {
            arr[i] = Integer.parseInt(parts[i]);
        }
        return arr;
    }

    public static void stringStats(String s, String s2) {
        String[] parts = s.split(" ");
        int count = parts.length;
        int maxLength = parts[0].length();
        int minLength = parts[0].length();
        for (int i = 1; i < parts.length; i++) {
            if (parts[i].length() > maxLength) maxLength = parts[i].length();
            if (parts[i].length() < minLength) minLength = parts[i].length();
        }
        int n = 0;

        for (String part : parts) {
            if (part.equals(s2)) n++;
        }

        for (int i = 0; i < parts.length; i++) {
            System.out.println("Word " + (i + 1) + ": " + parts[i] + "\n");
        }
        System.out.println("Word amount " + count + "\nMax Length " + maxLength + "\nMin Length " + minLength + "\nAmount of entries " + n);
    }

    public static String multiplyString(String str, int N) throws NegativeException {
        if (N < 0) {
            throw new NegativeException("N is negative");
        }
        else if (N == 0) return ("");
        else {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < N; i++) {
                builder.append(str);
            }
            return builder.toString();
        }
    }

    public static int entriesCount(String str, String subStr) {
        str += " ";
        String parts[] = str.split(subStr);
        return parts.length - 1;
    }

    public static String numberReplace(String str) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            switch (str.charAt(i)){
                case '1': builder.append("один"); break;
                case '2': builder.append("два"); break;
                case '3': builder.append("три"); break;
                default: builder.append(str.charAt(i));
            }
        }
        return builder.toString();
    }

    public static StringBuilder deleteChar(StringBuilder builder) {
        int halfLength = builder.length() / 2 + 1;
        for (int i = 1; i < halfLength; i++) {
            builder.deleteCharAt(i);
        }
        return builder;
    }

    public static StringBuilder replaceWord(StringBuilder builder) {
        String parts[] = builder.toString().split(" ");

        int length = parts.length;

        String temp = parts[0];
        parts[0] = parts[length - 1];
        parts[length - 1] = temp;
        builder.delete(0,builder.length());

        for (int i = 0; i < length; i++) {
            if (!parts[i].equals(" ")) {
                if (i == length - 1) builder.append(parts[i]);
                else builder.append(parts[i] + " ");
            }
        }
        return builder;
    }

}
