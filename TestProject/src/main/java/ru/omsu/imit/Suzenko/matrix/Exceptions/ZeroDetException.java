package ru.omsu.imit.Suzenko.matrix.Exceptions;

/**
 * Created by secto on 15.03.2017.
 */
public class ZeroDetException extends Exception{
    public ZeroDetException(String message){
        super(message);
    }
}
