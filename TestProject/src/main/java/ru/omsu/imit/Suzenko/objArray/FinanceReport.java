package ru.omsu.imit.Suzenko.objArray;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad on 02.12.2016.
 */
public class FinanceReport {
    private List<Payment> payments = new ArrayList<Payment>();
    public FinanceReport() {
    }


    public FinanceReport(FinanceReport fReport){
        for (Payment item : fReport.payments) {
            this.payments.add(new Payment(item));
        }
    }

    public void addPayment() {

        Payment payment = new Payment();
        CreatePayment.makePayment(payment);
        payments.add(payment);
    }

    public void addPayment(Payment payment) {
        payments.add(payment);
    }

    public void addPayment(int i) {
        CreatePayment.makePayment(payments.get(i));
    }


    public int amountOfPayments() {
        return payments.size();
    }

    public void changePayment(){
        int i = CreatePayment.getIndex();
        if (i <= amountOfPayments() -1) addPayment(i);
        else System.out.println("Error! Out of array range");
    }

    public void showPayment(){
        int i = CreatePayment.getIndex();
        if (i <= amountOfPayments() - 1) System.out.println(payments.get(i));
        else System.out.println("Error! Out of array range");
    }

    public void showPayment(int i) {
        System.out.println(payments.get(i));
    }

    /*public void showPayments(){
        for (int i = 0; i < payments.size(); i++){
        payments.get(i).toString();
        }
    }*/
    public void showPayments(){
        for (int i = 0; i < payments.size(); i++){
            System.out.println(payments.get(i));
        }
    }


    public void charPayment(char ch1){
        for (Payment item : payments) {
            if (item.getFIO().charAt(0) == ch1) {
                int count = 0;
                count++;
                showPayment(count - 1);
            }
        }
    }

    public void lessPayments(int amount){
        int count = 0;
        for (Payment item : payments) {
             count++;
             if (amount > item.getAmountOfPay()) showPayment(count - 1);
        }
    }
}
