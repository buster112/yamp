package ru.omsu.imit.Suzenko.math;
import ru.omsu.imit.Suzenko.math.Calculation;
import java.util.Scanner;

/**
 * Created by secto on 17.09.2016.
 */
public class TaylorMain {
    public static void main(String[] args) {

     //   Calculation calc = new Calculation();
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the power of EXP: ");
        double power = sc.nextDouble();
        System.out.println("Enter the accuracy: ");
        double accuracy = sc.nextDouble();
        double answer = Calculation.taylorCalc(power,accuracy);
        System.out.println(answer);
    }

}
