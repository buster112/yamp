package ru.omsu.imit.Suzenko.constructors3D.vector;

import ru.omsu.imit.Suzenko.constructors3D.point.Point3D;

/**
 * Created by Vlad on 03.10.2016.
 */
public class Vector3DArray {
    private Vector3D[] vectorArr;

    public Vector3DArray(int n) {
        vectorArr = new Vector3D[n];
        for (int i = 0; i < n; i++) {
            vectorArr[i] = new Vector3D();
        }
    }

    public Vector3D[] getArr() {
        return vectorArr;
    }

    public int arrayLength() {
        return vectorArr.length;
    }

    public void elemReplace(int n, Vector3D v1) {
        vectorArr[n] = v1;
    }

    public double maxVectorSize() {
        double maxSize = vectorArr[0].distance();

        for (int i = 0; i < vectorArr.length; i++ ) {
            if (vectorArr[i].distance() > maxSize) maxSize = vectorArr[i].distance();
        }
        return  maxSize;
    }

    public int findVector(Vector3D v1) {
        int check = -1;
        for (int i = 0; i < vectorArr.length; i++) {
            if (v1.equals(vectorArr[i])) return i;
        }
       return check;
    }

    public Vector3D arrVectorSum() {
        Vector3D vSum = new Vector3D();
        for (int i = 0; i < vectorArr.length; i++) {
            vSum = Vector3DProcessor.vectorSum(vSum, vectorArr[i]);
        }
        return vSum;
    }

    public Vector3D linearCombination(double[] array) {
        Vector3D answer = new Vector3D();
        double x,y,z;
        if (array.length != vectorArr.length) return answer;
        else {
            for (int i = 0; i < vectorArr.length; i++) {
                x = vectorArr[i].getX();
                y = vectorArr[i].getY();
                z = vectorArr[i].getZ();
                Vector3D tempVector = new Vector3D(x * array[i] ,y * array[i] ,z * array[i]);
                answer = Vector3DProcessor.vectorSum(answer,tempVector);
            }
        }
        return answer;
    }

    public Point3D[] shiftPoint(Point3D p1) {
        Point3D[] pointArr = new Point3D[vectorArr.length];
        double x = p1.getX();
        double y = p1.getY();
        double z = p1.getZ();
        for (int i = 0; i < vectorArr.length; i++) {
            double x1 = vectorArr[i].getX();
            double y1 = vectorArr[i].getY();
            double z1 = vectorArr[i].getZ();
            pointArr[i] = new Point3D((x + x1),(y + y1),(z + z1));
        }
        return pointArr;
    }
}
