package ru.omsu.imit.Suzenko.functions;

import ru.omsu.imit.Suzenko.exceptions.OverstepException;

/**
 * Created by secto on 12.04.2017.
 */
public interface Function {
    double function(double x) throws OverstepException;
    double getA();
    double getB();
}
