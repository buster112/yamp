package ru.omsu.imit.Suzenko.functional;

import ru.omsu.imit.Suzenko.functions.Function;
import ru.omsu.imit.Suzenko.exceptions.OverstepException;


/**
 * Created by secto on 19.04.2017.
 */
public interface Functional {
    double functional(Function function) throws OverstepException;
}
