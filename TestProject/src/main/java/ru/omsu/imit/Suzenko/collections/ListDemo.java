package ru.omsu.imit.Suzenko.collections;
import ru.omsu.imit.Suzenko.functional.Integral;

import java.util.*;

/**
 * Created by secto on 24.05.2017.
 */
public class ListDemo {
    /*
        Вход: список строк и символ.
        Выход: количество строк входного списка, у которых первый символ совпадает с заданным.
    */
    public static int countOfEqualSymbolStrings(List<String> list, char letter){
        int count = 0;
        for(int i = 0; i < list.size(); i++){
            if(list.get(i).charAt(0) == letter) count++;
        }
        return count;
    }

    /*
        Создайте класс Human с полями: фамилия, имя, отчество, возраст и методами:
        конструкторы, геттеры/сеттеры, equals и hashCode.
        Напишите метода класса ListDemo, который получает на вход список объектов типа Human
        и еще один объект типа Human.
        Результат — список однофамильцев заданного человека среди людей из входного списка.
     */

    public static List<Human> ListOfEqualSurnames(List<Human> list, Human human){
        List<Human> list1 = new ArrayList<>();
        for(int i = 0; i < list.size(); i++){
            if (list.get(i).getSurname().equals(human.getSurname())) {
                list1.add(list.get(i));
            }
        }
        return list1;
    }

    /*
        3. Вход: список объектов типа Human и еще один объект типа Human.
        Выход — копия входного списка, не содержащая выделенного человека.
        При изменении элементов входного списка элементы выходного изменяться не должны.
    */

    public static List<Human> ListWithoutHuman(List<Human> list, Human human){
        List<Human> list1 = new ArrayList<>();
        for (int i = 0; i < list.size(); i++){
            if (!list.get(i).equals(human)) {
                list1.add(list.get(i));
            }
        }
        return list1;
    }

    /*
        4. Вход: список множеств целых чисел и еще одно множество.
        Выход: список всех множеств входного списка, которые не пересекаются с заданным множеством.
    */
    public static List<Set<Integer>> notIntersectedSets(List<Set<Integer>> setsList, Set<Integer> set){
        boolean flag;
        List<Set<Integer>> result = new ArrayList<>();
        for(Set<Integer> item:setsList){
            flag = true;
            for (Integer number:item){
                if (set.contains(number)){
                    flag = false;
                }
            }
            if (flag){
                result.add(item);
            }
        }
        return result;
    }

   /*
	    Имеется набор людей, каждому человеку задан уникальный целочисленный идентификатор.
	    Напишите метод, который получает на вход отображение (Map) целочисленных идентификаторов в объекты типа Human
	    и множество целых чисел.
	    Результат — множество людей, идентификаторы которых содержатся во входном множестве.
   */

    public static Set<Human> choosePeopleWithIndexes(Map<Integer, Human> indexation, Set<Integer> indexes) {
        Set<Human> result = new HashSet<>();

        for (int k : indexes) {
            if (indexation.containsKey(k)) {
                result.add(indexation.get(k));
            }
        }

        return result;
    }

    /*
	    Для отображения из задачи 7 постройте список идентификаторов людей, чей возраст не менее 18 лет.
	 */
    public static List<Integer> buildListGreaterOrEqualTo18(Map<Integer, Human> indexation) {
        ArrayList<Integer> result = new ArrayList<>(indexation.size());

        for (Map.Entry<Integer, Human> entry : indexation.entrySet()) {
            if (entry.getValue().getAge() >= 18) {
                result.add(entry.getKey());
            }
        }

        return result;
    }

}
