package ru.omsu.imit.Suzenko.generics;

/**
 * Created by secto on 17.12.2016.
 */
public class Circle extends Figure {
    private double r;

    public Circle(double r){
        super(Math.PI * r * r);
    }
}
