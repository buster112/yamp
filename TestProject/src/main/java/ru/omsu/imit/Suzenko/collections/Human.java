package ru.omsu.imit.Suzenko.collections;

import java.util.Objects;

/**
 * Created by secto on 24.05.2017.
 */
public class Human {
    private String name;
    private String patronymic;
    private String surname;
    private int age;
    private int ID;

    public Human(String name, String patronymic,String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.age = age;
    }

    public Human(String name, String surname){
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;

        Human human = (Human) o;

        if (getAge() != human.getAge()) return false;
        if (!getName().equals(human.getName())) return false;
        if (!getSurname().equals(human.getSurname())) return false;
        return getPatronymic().equals(human.getPatronymic());

    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getSurname().hashCode();
        result = 31 * result + getPatronymic().hashCode();
        result = 31 * result + getAge();
        return result;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", secondName='" + surname + '\'' +
                ", fathersName='" + patronymic + '\'' +
                ", age=" + age +
                '}';
    }
}
