package ru.omsu.imit.Suzenko.matrix;

import ru.omsu.imit.Suzenko.matrix.Exceptions.ZeroDetException;

/**
 * Created by Vlad on 14.03.2017.
 */
public class InvertableMatrix extends Matrix implements IInvertableMatrix {
    private double[] invertableMatrix;

    public InvertableMatrix(int n) {
        super(n);
        invertableMatrix = new double[n * n];
    }



    public void setInvMatrix(double[] arr){
        for (int i = 0; i < getSize(); i++){
            for (int j = 0; j < getSize(); j++){
                invertableMatrix[getSize()*i+j] = arr[getSize()*i+j];
            }
        }
    }

    public void showInvMatrix(){
        for (int i = 0; i < getSize(); i++){
            for (int j = 0; j < getSize(); j++){
                System.out.print(invertableMatrix[getSize()*i+j] + " ");
            }
            System.out.println();
        }
    }

    public double getInvElem(int i, int j){
        int index = getSize() * i + j;
        return invertableMatrix[index];
    }

    private double[] setSingleMatrix(double[] matrix){
        for (int i = 0; i < getSize(); i++) {
            matrix[getSize() * i + i] = 1;
        }
        return matrix;
    }

    private double[] makeTopTriangle(double[] matrix) throws Exception{
        double temp;
        for (int x = 0; x < getSize() - 1; x++) {
            if (Math.abs(getTempElem(x, x)) <= EPS) {
                int check = findNonZeroString(x);
                if (check == -1) {
                    throw new ZeroDetException("Zero Determinant");
                }
                swapLines(x, check);
                for (int i = 0; i < getSize(); i++) {
                    temp = matrix[getSize() * x + i];
                    matrix[getSize() * x + i] = matrix[getSize() * check + i];
                    matrix[getSize() * check + i] = temp;
                }
            }

            for (int i = x + 1; i < getSize(); i++) {
                double coef = getTempElem(i, x) / getTempElem(x, x);
                gauss(x, i, coef);
                for (int j = 0; j < getSize(); j++) {
                    matrix[getSize() * i + j] -= matrix[getSize() * x + j] * coef;
                }
            }
        }
        return matrix;
    }

    private double[] makeBottomTriangle(double[] matrix){
        double temp;
        for (int i = 0; i < getSize(); i++){
            if (Math.abs(getTempElem(i, i)) - 1 > EPS || getTempElem(i, i) < 0){
                temp = getTempElem(i, i);
                for (int j = 0; j < getSize(); j++){
                    matrix[getSize() * i + j] /= temp;
                    matrixCopy[getSize() * i + j] /= temp;
                }
            }
        }

        for (int x = getSize() - 1; x > 0; x--) {
            for (int i = x - 1; i >= 0; i--) {
                double a = getTempElem(i, x) / getTempElem(x, x);
                gauss(x, i, a);
                for (int j = 0; j < getSize(); j++) {
                    matrix[getSize() * i + j] -= matrix[getSize() * x + j] * a;
                }
            }
        }
        return matrix;
    }

    public IMatrix getInvertedMatrix() throws Exception {

        double[] result = new double[getSize() * getSize()];
        if (Math.abs(getDeterminant()) <= EPS) throw new ZeroDetException("Zero Determinant");

        setSingleMatrix(result);
        copyMatrix();
        makeTopTriangle(result);
        makeBottomTriangle(result);
        setInvMatrix(result );
        Matrix invMatrix = new Matrix(getSize(), invertableMatrix);
        return invMatrix;
    }
}