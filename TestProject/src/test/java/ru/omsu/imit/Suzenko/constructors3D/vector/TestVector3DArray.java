package ru.omsu.imit.Suzenko.constructors3D.vector;

import org.junit.Test;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by Vlad on 03.10.2016.
 */
public class TestVector3DArray {
    Vector3DArray arr = new Vector3DArray(5);
    Vector3D[] array = arr.getArr();
    Vector3D v1 = new Vector3D(12,52,2);
    Vector3D v2 = new Vector3D(-1,2,-1337);
    Vector3D vSumm = new Vector3D(11,54,-1335);
    Vector3D v3 = new Vector3D(12,11,22);

    Vector3D vv1 = new Vector3D(2, -1, 0);
    Vector3D vv2 = new Vector3D(-3, 2, 1);
    Vector3D vv3 = new Vector3D(-5, 4, 3);
    Vector3D vv4 = new Vector3D();
    Vector3DArray arr2 = new Vector3DArray(2);

    @Test
    public void lengthTest() {
        assertEquals(5,arr.arrayLength());

    }
    @Test
    public void simpleTest(){

        arr.elemReplace(2,v1);
        arr.elemReplace(3,v2);
        arr.elemReplace(4,v1);
        array[2].equals(v1);
        array[3].equals(v2);

        assertEquals(1337, arr.maxVectorSize(),0.1);

        assertEquals(2, arr.findVector(v1));
        assertEquals(-1, arr.findVector(v3));

        vSumm.equals(arr.arrVectorSum());


    }

    @Test
    public void linearCombTest() {
        arr2.elemReplace(0, vv1);
        arr2.elemReplace(1, vv2);
        double coeff[] = {2, 3};
        Vector3D check = arr2.linearCombination(coeff);
        check.equals(vv3);
        double wrongCoeff[] = {2, 3, 4};
        Vector3D check2 = arr2.linearCombination(wrongCoeff);
        check2.equals(vv4);
    }
}
