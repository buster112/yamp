package ru.omsu.imit.Suzenko.functional;

import org.junit.Test;
import ru.omsu.imit.Suzenko.exceptions.OverstepException;
import ru.omsu.imit.Suzenko.functions.*;

import static junit.framework.TestCase.assertEquals;
/**
 * Created by secto on 19.04.2017.
 */
public class FunctionalSumTest {
    Functional fs = new FunctionalSum();
    Function sine = new Sine(0, 2, 2, 7);
    Function pol1 = new Polynom(0, 2, 2, 7);
    Function polD = new PolynomDivide(0, 2, 1, 1, 1, 1);
    Function exp = new Exp(0, 50, 2, 7);

    @Test
    public void sumTest() throws OverstepException{
        assertEquals(3.0 , fs.functional(polD));
    }
}
