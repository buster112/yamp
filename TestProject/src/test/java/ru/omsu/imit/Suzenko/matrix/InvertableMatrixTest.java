package ru.omsu.imit.Suzenko.matrix;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by secto on 15.03.2017.
 */
public class InvertableMatrixTest{

    double[] arr = {0, 1, 1, 1, 1, 2, 1, 1, 1};
    InvertableMatrix m1 = new InvertableMatrix(3);
    @Test
    public void testInvMatrix() throws Exception{
        m1.setMatrix(arr);
        m1.getInvertedMatrix();
        m1.showInvMatrix();
        assertEquals(m1.getInvElem(0,0), -1.0);
        assertEquals(m1.getInvElem(0,1), 0.0);
        assertEquals(m1.getInvElem(0,2), 1.0);
        assertEquals(m1.getInvElem(1,0), 1.0);
        assertEquals(m1.getInvElem(1,1), -1.0);
        assertEquals(m1.getInvElem(1,2), 1.0);
        assertEquals(m1.getInvElem(2,0), -0.0);
        assertEquals(m1.getInvElem(2,1), 1.0);
        assertEquals(m1.getInvElem(2,2), -1.0);
    }
}
