package ru.omsu.imit.Suzenko.functional;

import ru.omsu.imit.Suzenko.functions.Sine;
import org.junit.Test;
import ru.omsu.imit.Suzenko.exceptions.OverstepException;
import ru.omsu.imit.Suzenko.functions.*;
import static junit.framework.TestCase.assertEquals;
/**
 * Created by secto on 19.04.2017.
 */
public class IntegralTest {
    final double EPS = 0.001;
    Integral integral = new Integral();
    Function sin = new Sine(0, 2, 9, 1);
    Function exp = new Exp(0, 1 , 1, 0);
    Function pol = new Polynom(3, 4, 5, 7);
    Function polD = new PolynomDivide(0, 2, 1, 2, 4, 9);


    @Test
    public void integralSineTest() throws OverstepException {
        assertEquals(12.745, integral.functional(sin), EPS);
    }

    @Test
    public void integralExpTest() throws OverstepException {
        assertEquals(Math.E - 1, integral.functional(exp), EPS);
    }

    @Test
    public void polynomTest() throws OverstepException {
        assertEquals(24.5, integral.functional(pol), EPS);
    }

    @Test
    public void polynomDivTest() throws OverstepException {
        assertEquals(0.46025, integral.functional(polD), EPS);
    }


}
