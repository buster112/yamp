package ru.omsu.imit.Suzenko.collections;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by secto on 24.05.2017.
 */
public class ListDemoTest {
    List<String> arrayList = new ArrayList<>();
    List<Human> humans = new ArrayList<>();

    Human human1 = new Human("Василий", "Петрович" , "Максимов", 54);
    Human human2 = new Human("11","22","33",44 );
    Human human3 = new Human("421", "213", "33", 12);
    Human human4 = new Human("Александр", "Петрович", "Максимов", 45);

    Human human = new Human("Петр", "Сергеевич", "Максимов", 70);
    public void createList(){
        arrayList.add("тестовая строка");
        arrayList.add("test String");
        arrayList.add("а");
        arrayList.add("алабаваф");
    }

    public void createHumans(){
        humans.add(human1);
        humans.add(human2);
        humans.add(human3);
        humans.add(human4);
    }

    @Test
    public void testCount(){
        createList();
        assertEquals(1, ListDemo.countOfEqualSymbolStrings(arrayList,'т'));
        assertEquals(2, ListDemo.countOfEqualSymbolStrings(arrayList, 'а'));
        assertEquals(0, ListDemo.countOfEqualSymbolStrings(arrayList, 'р'));
    }

    @Test
    public void testHumans() {
        createHumans();
        List<Human> testList = new ArrayList<>();
        testList.add(human1);
        testList.add(human4);
        assertTrue(testList.equals(ListDemo.ListOfEqualSurnames(humans,human)));
        assertEquals(testList, ListDemo.ListOfEqualSurnames(humans,human));
    }

    @Test
    public void testDeleteHuman(){
        createHumans();
        List<Human> testList = new ArrayList<>();
        testList.add(human1);
        testList.add(human3);
        testList.add(human4);
        assertEquals(testList, ListDemo.ListWithoutHuman(humans, human2));

    }

    @Test
    public void notIntersectionTest(){
        ArrayList<Set<Integer>> people= new ArrayList<>();
        HashSet<Integer> tmp= new HashSet<>();
        ListDemo list=new ListDemo();

        tmp.add(1);
        tmp.add(2);
        tmp.add(3);
        people.add(tmp);


        HashSet<Integer> tmp1= new HashSet<Integer>();
        tmp1.add(4);
        tmp1.add(5);
        tmp1.add(6);
        people.add(tmp1);

        HashSet<Integer> tmp2= new HashSet<Integer>();
        tmp2.add(6);
        tmp2.add(7);
        tmp2.add(3);
        people.add(tmp2);

        HashSet<Integer> tmp3= new HashSet<Integer>();
        tmp3.add(13);
        tmp3.add(8);
        tmp3.add(9);
        people.add(tmp3);

        List<HashSet<Integer>> result= new ArrayList<HashSet<Integer>>();
        HashSet<Integer> tmp4= new HashSet<Integer>();
        tmp4.add(1);
        tmp4.add(2);
        tmp4.add(3);
        result.add(tmp4);

        HashSet<Integer> tmp5= new HashSet<Integer>();
        tmp5.add(13);
        tmp5.add(8);
        tmp5.add(9);
        result.add(tmp5);

        Set<Integer> tmp6= new HashSet<>();
        tmp6.add(6);
        tmp6.add(18);
        tmp6.add(32);


        List<Set<Integer>>  act= list.notIntersectedSets(people, tmp6);
        assertEquals(result, act);

    }
    
    
}
