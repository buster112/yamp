package ru.omsu.imit.Suzenko.constructors3D.vector.point;
import org.junit.Test;
import ru.omsu.imit.Suzenko.constructors3D.point.Point3D;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by secto on 01.10.2016. TextForCommit
 */
public class TestPoint3D {
    Point3D p1 = new Point3D();
    Point3D p2 = new Point3D();
    Point3D p3 = new Point3D(3.12,-2131.21322,0);
    @Test
    public void programTest() {
        assertEquals(0.0,p1.getX());
        assertEquals(true,p1.equals(p2));
        assertEquals(false,p1.equals(p3));
        assertEquals(-2131.21322,p3.getY());
    }
}

/**
 *    textForCommit...
 */
