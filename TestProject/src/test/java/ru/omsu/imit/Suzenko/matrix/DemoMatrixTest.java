package ru.omsu.imit.Suzenko.matrix;

import org.junit.Test;
import java.io.IOException;
import static junit.framework.TestCase.assertEquals;
/**
 * Created by secto on 22.03.2017.
 */
public class DemoMatrixTest {
    final double EPS = 0.000001;

    @Test
    public void testFile() throws IOException {
        double[] arr = {0,1,1,1,0,99,1,1,1};
        Matrix m1 = new Matrix(3, arr);
        Matrix m2 = new Matrix(3);
        DemoMatrix.writeTextFile(m1);
        DemoMatrix.readTextFile(m2);
        m1.getDeterminant();
        m2.getDeterminant();
        assertEquals(m1.getElem(0,0), m2.getElem(0,0), EPS );
        assertEquals(m1.getDeterminant(), m2.getDeterminant(), EPS);
        assertEquals(true, m1.equals(m2));
    }
    @Test
    public void testSumElem(){
        double[] arr = {0,3,3,3,1,9,2,1,5};
        Matrix m = new Matrix(3, arr);
        double res = DemoMatrix.sumOfElems(m);
        assertEquals(27, res, EPS);
    }
    @Test
    public void testSerialisation() throws IOException, ClassNotFoundException {
        double[] arr = {0,1,1,1,0,99,1,1,1};
        Matrix m1 = new Matrix(3, arr);
        Matrix m2 = new Matrix(3);
        m1.getDeterminant();
        DemoMatrix.serialize(m1);
        m2 = DemoMatrix.deSerialize();
        assertEquals(m1.getDeterminant(), m2.getDeterminant(), EPS);
        assertEquals(m1.getElem(1,2), m2.getElem(1,2), EPS);
        assertEquals(m1.getElem(0,2), m2.getElem(0,2), EPS);
        assertEquals(m1.getElem(2,2), m2.getElem(2,2), EPS);
        assertEquals(true, m1.equals(m2));
    }
}
