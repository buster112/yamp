package ru.omsu.imit.Suzenko.arrays;
import org.junit.*;
import org.junit.Assert;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by secto on 24.09.2016.
 */
public class arraysTest {
    @Test

    public void sumTest() {
        Arrays arr = new Arrays();
        int[] testArray1 = {1,2,3,4,5};
        int[] testArray2 = {-2,0,0,2,1222};
        assertEquals((1+2+3+4+5), arr.ArrSum(testArray1));
        assertEquals((-2+2+1222), arr.ArrSum(testArray2));
    }

    @Test
    public void evenTest() {
        Arrays arr = new Arrays();
        int[] testArray1 = {1,2,3,4,5};
        int evenCount1 = 2;
        int[] testArray2 = {1,3,5,7,0};
        int evenCount2 = 1;
        assertEquals(evenCount1,arr.CheckEven(testArray1));
        assertEquals(evenCount2,arr.CheckEven(testArray2));
    }

    @Test
    public void segmentTest() {
        Arrays arr = new Arrays();
        int[] testArray1 = {1,2,3,4,5};
        int a1 = -5, b1 = 3;
        int segmentCount1 = 3;
        assertEquals(segmentCount1,arr.IsItInAB(testArray1,a1,b1));
        int[] testArray2 = {0,2,3,4,5};
        int a2 = 0, b2 = 0;
        int segmentCount2 = 1;
        assertEquals(segmentCount2,arr.IsItInAB(testArray2,a2,b2));
    }

    @Test
    public void positiveTest() {
        Arrays arr = new Arrays();
        int[] testArray1 = {-1,2,-3,4,5};
        int[] testArray2 = {1,2,3,4,5};
        boolean isPos1 = false, isPos2 = true;
        assertEquals(isPos1,arr.isPositive(testArray1));
        assertEquals(isPos2,arr.isPositive(testArray2));
    }

    @Test
    public void inverseTest() {
        Arrays arr = new Arrays();
        int[] testArray1 = {-1,2,-3,4,5};
        int[] revArr1 = {5,4,-3,2,-1};
        Assert.assertArrayEquals(revArr1,arr.ReverseArr(testArray1));
    }
}
