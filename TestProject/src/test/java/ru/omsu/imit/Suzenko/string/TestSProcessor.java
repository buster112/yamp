package ru.omsu.imit.Suzenko.string;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.Suzenko.exceptions.NegativeException;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by Vlad on 19.10.2016.
 */
public class TestSProcessor {

    String s = "123456";
    String s1 = "Hello from the other side";
    @Test
    public void testRevert() {
        assertEquals("654321",StringProcessor.revert(s));
        System.out.println(StringProcessor.revert(s1));
    }

    @Test
    public void testIntToArr() {
        int[] arr = new int[5];
        for (int i = 0; i < arr.length; i++){
            arr[i] = i + 1;
        }
        System.out.println(StringProcessor.intArrToString(arr));
        assertEquals("1 2 3 4 5",StringProcessor.intArrToString(arr));
    }

    @Test
    public void testStrToArr() {
        String str = "1 2 35 4 5";
        int arr[] = {1,2,35,4,5};
        Assert.assertArrayEquals(arr,StringProcessor.stringToArr(str));
    }

    @Test
    public void testStats() {
        String str1 = "Привет мне 19 лет";
        String str2 = "мне";
        StringProcessor.stringStats(str1,str2);
    }

    @Test (expected = NegativeException.class)
    public void testMultiply() throws Exception {
        String str1 = "copy ";
        System.out.println(StringProcessor.multiplyString(str1, 7));
        System.out.println(StringProcessor.multiplyString(str1, 0));
        System.out.println(StringProcessor.multiplyString(str1, -7));
    }

    @Test
    public void testEntries() {
        String str = "qwertyqwerty11w2";
        String subStr = "qw";
        String subStr2 = "2";
        String subStr3 = "";
        assertEquals(2,StringProcessor.entriesCount(str,subStr));
        assertEquals(1,StringProcessor.entriesCount(str,subStr2));
        assertEquals(str.length(),StringProcessor.entriesCount(str,subStr3));
    }

    @Test
    public void testReplace() {
        String str = "12345231";
        assertEquals("одиндватри45дватриодин", StringProcessor.numberReplace(str));
    }

    @Test
    public void testCharDelete() {
        StringBuilder sb = new StringBuilder("121212121");
        StringBuilder sb1 = new StringBuilder("");
        assertTrue("11111".equals(StringProcessor.deleteChar(sb).toString()));
        assertTrue(sb1.equals(StringProcessor.deleteChar(sb1)));
    }

    @Test
    public void testWordReplace() {
        StringBuilder sb = new StringBuilder("Hello my name is Vlad");
        StringBuilder sb1 = new StringBuilder("    1 2   3 4 5 6 7 8 9");
        //"Vlad my name is Hello".equals(StringProcessor.replaceWord(sb));
        //"9 2 3 4 5 6 7 8 1".equals(StringProcessor.replaceWord(sb1));
        System.out.println(StringProcessor.replaceWord(sb));
        System.out.println(StringProcessor.replaceWord(sb1));
    }
}
