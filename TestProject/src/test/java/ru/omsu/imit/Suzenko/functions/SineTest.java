package ru.omsu.imit.Suzenko.functions;

import org.junit.Test;
import ru.omsu.imit.Suzenko.exceptions.OverstepException;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by secto on 12.04.2017.
 */
public class SineTest {
    Sine sine = new Sine(0, 2, 2, 7);
    Sine sine2 = new Sine(0, 1, 2, 7);

    @Test
    public void getATest(){
        assertEquals(0.0, sine.getA());
    }

    @Test
    public void getBTest(){
        assertEquals(2.0, sine.getB());
    }

    @Test
    public void functionTest() throws OverstepException {
        assertEquals(2 * Math.sin(14), sine.function(2));
    }

    @Test(expected = OverstepException.class)
    public void functionErrorTest() throws OverstepException {
        sine2.function(2);
    }
}

