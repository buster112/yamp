package ru.omsu.imit.Suzenko.constructors3D.vector;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;


/**
 * Created by secto on 01.10.2016.
 */

public class VectorProcessorTest {
        Vector3D v1 = new Vector3D();
        Vector3D v2 = new Vector3D(1, 2, 3);
        Vector3D v3 = new Vector3D(3, -6, 9);
        Vector3D v4 = new Vector3D(3, 6, 9);
        Vector3D sum = new Vector3D(4,-4,12);
        Vector3D sub = new Vector3D(2,8,6);
        Vector3D vectCom = new Vector3D(36,0,-12);

    @Test
    public void sumTest() {
        sum.equals(Vector3DProcessor.vectorSum(v2,v3));
    }

    @Test
    public void subTest() {
        sub.equals(Vector3DProcessor.vectorSubtraction(v3,v2));
    }

    @Test
    public void scalarTest() {
        assertEquals(18.0, Vector3DProcessor.scalarComposition(v2,v3));

    }

    @Test
    public void vectorTest() {
        v1.equals(Vector3DProcessor.vectorComposition(v2,v3));
        vectCom.equals(Vector3DProcessor.vectorComposition(v2,v3));
    }

    @Test
    public void collinearTest() {
        assertEquals(true, Vector3DProcessor.isCollinear(v2,v4));
        assertEquals(false, Vector3DProcessor.isCollinear(v2,v3));
    }
}
