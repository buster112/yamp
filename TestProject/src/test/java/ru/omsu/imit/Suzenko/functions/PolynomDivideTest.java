package ru.omsu.imit.Suzenko.functions;

import org.junit.Test;
import ru.omsu.imit.Suzenko.exceptions.OverstepException;
import static junit.framework.TestCase.assertEquals;
/**
 * Created by secto on 12.04.2017.
 */
public class PolynomDivideTest {
    PolynomDivide polD = new PolynomDivide(0, 10, 2, 7, -5, 12);
    PolynomDivide polD2 = new PolynomDivide(0, 2, 2, 7, -5, 12);

    @Test
    public void getATest(){
        assertEquals(0.0, polD.getA());
    }

    @Test
    public void getBTest(){
        assertEquals(10.0, polD.getB());
    }

    @Test
    public void functionTest() throws OverstepException {
        assertEquals(5.5, polD.function(2));
    }

    @Test(expected = OverstepException.class)
    public void functionErrorTest() throws OverstepException {
        polD2.function(2);
    }
}
