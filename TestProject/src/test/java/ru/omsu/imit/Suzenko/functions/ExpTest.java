package ru.omsu.imit.Suzenko.functions;

import org.junit.Test;
import ru.omsu.imit.Suzenko.exceptions.OverstepException;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by secto on 12.04.2017.
 */
public class ExpTest {
    Exp exp = new Exp(0, 50, 2, 7);
    Exp exp2 = new Exp(0, 2, 2, 7);


    @Test
    public void getATest(){
        assertEquals(0.0, exp.getA());
    }

    @Test
    public void getBTest(){
        assertEquals(50.0, exp.getB());
    }

    @Test
    public void functionTest() throws Exception{
        assertEquals(2 * Math.exp(2) + 7, exp.function(2));
    }

    @Test(expected = OverstepException.class)
    public void functionErrorTest() throws Exception{
        exp2.function(2);
    }

}
