package ru.omsu.imit.Suzenko.functions;

import org.junit.Test;
import ru.omsu.imit.Suzenko.exceptions.OverstepException;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by secto on 12.04.2017.
 */
public class PolynomTest {
    Polynom pol1 = new Polynom(0, 2, 2, 7);
    Polynom pol2 = new Polynom(0, 20, 2, 7);


    @Test
    public void getATest(){
        assertEquals(0.0, pol1.getA());
    }

    @Test
    public void getBTest(){
        assertEquals(2.0, pol1.getB());
    }

    @Test(expected = OverstepException.class)
    public void functionErrorTest() throws Exception{
        pol1.function(2);
    }

    @Test
    public void functionTest() throws Exception{
        assertEquals(11.0, pol2.function(2));
    }
}
