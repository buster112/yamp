package ru.omsu.imit.Suzenko.reflections;

import org.junit.Test;
import ru.omsu.imit.Suzenko.collections.Human;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Vlad on 06.06.2017.
 */
public class ReflectionDemoTest {

    @Test
    public void findAdultPeopleTest() {
        ReflectionDemo loki = new ReflectionDemo();


        List<Object> identifier = new ArrayList<>();
        identifier.add(new Human("Максим", "Максимович", "Максимов", 1000000));
        identifier.add(1);
        identifier.add(new Human("Андрей", "Андреевич", "Андреев", 16));
        identifier.add("I'm Groot");
        identifier.add(new Human("Александр", "Александрович", "Александров", 15));
        identifier.add("PINEAPLE");

        int res = loki.countPeople(identifier);
        assertEquals(3, res);
    }

   /* @Test
    public void findAdultPeopleTest2() {
        ReflectionDemo loki = new ReflectionDemo();

        Map<String, List<String>> book = new HashMap<>();

        List<String> phone1 = new ArrayList<>();
        phone1.add("89604637271");
        phone1.add("355821");
        book.put("Thor", phone1);


        List<String> phone2 = new ArrayList<>();
        phone2.add("89562112342");
        phone2.add("234363");
        phone2.add("8957298341");
        book.put("Loki", phone2);
        PhoneBook ron = new PhoneBook(book);

        List<String> res = loki.findPublicMethuds(ron);

        List<String> act = new ArrayList<>();
        act.add("equals");
        act.add("hashCode");
        act.add("deletePhone");
        act.add("getAllManPhone");
        act.add("addPhone");

        assertEquals(act, res);
    } */
}
