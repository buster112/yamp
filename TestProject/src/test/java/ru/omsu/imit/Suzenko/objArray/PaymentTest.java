package ru.omsu.imit.Suzenko.objArray;

import static junit.framework.TestCase.assertEquals;
import org.junit.Test;

/**
 * Created by Vlad on 02.12.2016.
 */
public class PaymentTest {

    @Test
    public void testGetters() {
        Payment pay1 = new Payment("Владислав", 24, 12, 2012, 12800);
        assertEquals("Владислав",pay1.getFIO());
        assertEquals(24, pay1.getDayOfPay());
        assertEquals(12, pay1.getMonthOfPay());
        assertEquals(2012, pay1.getYearOfPay());
        assertEquals(12800, pay1.getAmountOfPay());
    }

    @Test
    public void testSetters() {
        Payment pay1 = new Payment("Владислав", 24, 12, 2012, 12800);
        pay1.setFIO("Максим Анатольевич");
        assertEquals("Максим Анатольевич", pay1.getFIO());
        pay1.setDayOfPay(11);
        assertEquals(11, pay1.getDayOfPay());
        pay1.setMonthOfPay(1);
        assertEquals(1, pay1.getMonthOfPay());
        pay1.setYearOfPay(2016);
        assertEquals(2016,pay1.getYearOfPay());
        pay1.setAmountOfPay(13222);
        assertEquals(13222, pay1.getAmountOfPay());
    }

    @Test
    public void testEquals() {
        Payment pay1 = new Payment("Владислав", 24, 12, 2012, 12800);
        Payment pay2 = new Payment("Владислав", 24, 12, 2012, 12800);
        Payment pay3 = new Payment("Максим", 24, 12, 2012, 12800);
        assertEquals(true, pay1.equals(pay2));
        assertEquals(false, pay1.equals(pay3));
    }
}
