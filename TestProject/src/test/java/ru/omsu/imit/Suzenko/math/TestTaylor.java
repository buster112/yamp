package ru.omsu.imit.Suzenko.math;
import org.junit.*;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by secto on 17.09.2016.
 */
public class TestTaylor {
        @Test
                public void simpleTest() {
            Calculation calc = new Calculation();
            double power = 2;
            double accuracy = 0.001;
            assertEquals(calc.taylorCalc(power, accuracy), Math.E * Math.E , 0.001);

        }
    }
