package ru.omsu.imit.Suzenko.constructors3D.vector;
import org.junit.Test;
import ru.omsu.imit.Suzenko.constructors3D.point.Point3D;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by secto on 01.10.2016.
 */
public class TestVector3D {

    //@Before

    Point3D p1 = new Point3D(1, 2, 3);
    Point3D p2 = new Point3D(4, 5, 6);

    Vector3D v1 = new Vector3D();
    Vector3D v2 = new Vector3D(0.123,234.213,-23);
    Vector3D v3 = new Vector3D(p1,p2);
    Vector3D v4 = new Vector3D();

    @Test
    public void simpleTest() {
        assertEquals(true, v1.equals(v4));
        assertEquals(234.213,v2.getY());
        assertEquals(false,v2.equals(v3));
        assertEquals(5.196152, v3.distance(), 1e-4);
    }
}
