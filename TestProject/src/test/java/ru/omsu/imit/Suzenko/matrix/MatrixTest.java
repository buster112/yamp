package ru.omsu.imit.Suzenko.matrix;

import org.junit.Before;
import org.junit.Test;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by Vlad on 28.02.2017.
 */
public class MatrixTest {

    double [] arr = {0,1,1,6,3,1,2,1,1};
    Matrix m1;
    double [] arr2 = {0,1,1,0,3,1,0,1,1};
    Matrix m2;
    double [] arr3 = {1,2,3,0,0,4,0,0,7};
    Matrix m3;
    double [] arr4 = {1,2,3,0,2,4,0,0,3};
    Matrix m4;

    @Before
    public void init(){
        m1 = new Matrix(3, arr);
        m2 = new Matrix(3, arr2);
        m3 = new Matrix(3, arr3);
        m4 = new Matrix(3, arr4);
    }
    @Test
    public void detTest(){
        assertEquals(m1.getDeterminant(), -4.0, 0.00001);
    }

    @Test
    public void zeroDetTest(){
        assertEquals(m2.getDeterminant(), 0.0, 0.000001);
    }

    @Test
    public void detTest2(){
        assertEquals(m3.getDeterminant(), 0.0, 0.000001);
    }

    @Test
    public void detTest3(){
        assertEquals(m4.getDeterminant(),6.0,0.000001);
    }

    @Test
    public void getTest(){
        assertEquals(m1.getElem(2,2), 1.0, 0.00001);
    }

    @Test
    public void setTest(){
        m1.setElem(2, 2, 99);
        assertEquals(m1.getElem(2, 2), 99.0, 0.00001);
    }

    @Test
    public void copyMatrixTest(){
        m1.copyMatrix();
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                assertEquals(m1.getTempElem(i, j), m1.getElem(i, j), 0.00001);
            }
        }
    }

    @Test
    public void swapLinesTest(){
        m1.copyMatrix();
        m1.swapLines(0, 1);
        assertEquals(m1.getTempElem(0,0), 6.0, 0.00001);
        assertEquals(m1.getTempElem(1,1), 1.0, 0.00001);
        assertEquals(m1.getTempElem(0,2), 1.0, 0.00001);
    }

}
