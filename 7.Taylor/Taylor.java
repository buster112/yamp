 import java.util.Scanner; 
 import java.lang.Math;
 
  class Taylor {
	  public static void main (String[] args) {
		  Scanner sc = new Scanner(System.in);
		  double power, accuracy, f, answer;
		  double e = Math.E;
		  int a = 0;
		  
		  System.out.println("Enter the power of EXP: ");
		  power = sc.nextDouble();
		  System.out.println("Enter the accuracy: ");
		  accuracy = sc.nextDouble();
		  
		  answer = 0;
		  do  {
			  f = (Math.pow(e, 0) * Math.pow(power,a)) / fact(a) ;
			  answer += f;
			  a++;
		  }
		  while (Math.abs(f) > accuracy);
		  System.out.println(answer);
	  }
	  
	  public static double fact(int a) {
		  double answer;
		  if (a == 0) {
			  return(1);
		  } 
		  else {
		  answer = 1;
		  for (int i = 1 ; i <= a; i++) {
			  answer = answer * i; 
		  }
		  return(answer);
	  }
	  }
  }
  